package main

import (
	"fmt"
	"math/rand"
	"sync"

	"github.com/Necroforger/gomoji"
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

var entranceActions = []string{
	"enters",
	"saunters into",
	"struts into",
	"strolls into",
	"pounces into",
	"leaps into",
	"dashes into",
	"zooms into",
	"steps carefully into",
	"flops into",
}

var entranceDescriptions = []string{
	"",
	"quietly",
	"loudly",
	"confidently",
	"with a *meow*",
	"cutely",
	"all aloof",
	"sweetly",
	"gently",
	"stoicaly",
	"with purpose",
	"quickly",
	"sneakily",
}

var currentChannels sync.Map
var lockedDoors sync.Map

func isDoorLocked(channelID string) bool {
	_, ok := lockedDoors.Load(channelID)
	return ok
}

func lockDoor(channelID string) {
	lockedDoors.Store(channelID, true)
}

func unlockDoor(channelID string) {
	lockedDoors.Delete(channelID)
}

func getCurrentChannel(guildID string) (string, error) {
	channelRaw, ok := currentChannels.Load(guildID)
	if !ok {
		err := fmt.Errorf("No current channel for guild: %s", guildID)
		logrus.Error(err)
		return "", err
	}
	return channelRaw.(string), nil
}

func setCurrentChannel(guildID string, channelID string) {
	currentChannels.Store(guildID, channelID)
}

func enterRandomChannel(s *discordgo.Session, guildID string) error {
	allChannels, err := s.GuildChannels(guildID)
	if err != nil {
		logrus.Error(err)
		return err
	}
	var channels []*discordgo.Channel
	var textChannels []*discordgo.Channel

	for i := range allChannels {
		if allChannels[i].Type == discordgo.ChannelTypeGuildText {
			textChannels = append(textChannels, allChannels[i])
			if !isDoorLocked(allChannels[i].ID) {
				channels = append(channels, allChannels[i])
			}
		}
	}
	var message string
	var channel *discordgo.Channel

	currentChannel, err := getCurrentChannel(guildID)
	if err == nil {
		if isDoorLocked(currentChannel) {
			message = "_Waits patiently near the door_"
			s.ChannelMessageSend(currentChannel, message)
			return nil
		}
	}

	if len(channels) == 0 {
		message = "_Wanders in the hallway, unable to enter a room_"
		channel = textChannels[rand.Intn(len(textChannels))]
		channel.ID = ""
	} else {
		channel = channels[rand.Intn(len(channels))]
		logrus.Infof("Joining room %s for guild %s", channel.Name, channel.GuildID)

		message = fmt.Sprintf("_%s the room %s_",
			entranceActions[rand.Intn(len(entranceActions))],
			entranceDescriptions[rand.Intn(len(entranceDescriptions))])
	}
	setCurrentChannel(guildID, channel.ID)
	s.ChannelMessageSend(channel.ID, message)
	return nil
}

func meow(s *discordgo.Session, m *discordgo.MessageCreate) error {
	guildID := m.GuildID
	channelID, err := getCurrentChannel(guildID)
	if err != nil {
		return err
	}
	_, err = s.ChannelMessageSendTTS(channelID, "_meow_")
	if err != nil {
		logrus.Error(err)
	}
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":smirk_cat:"))
	return nil
}

func purr(s *discordgo.Session, m *discordgo.MessageCreate) error {
	guildID := m.GuildID
	channelID, err := getCurrentChannel(guildID)
	if err != nil {
		return err
	}
	s.ChannelMessageSendTTS(channelID, "_purr_")
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":heart_eyes_cat:"))
	return nil
}

func hide(s *discordgo.Session, m *discordgo.MessageCreate) error {
	guildID := m.GuildID
	channelID, err := getCurrentChannel(guildID)
	if err != nil {
		return err
	}
	s.ChannelMessageSend(channelID, "_hides somewhere in the room_")
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":pouting_cat:"))
	return nil
}

func inCurrentChannel(m *discordgo.MessageCreate) bool {
	channelID, err := getCurrentChannel(m.GuildID)
	if err != nil {
		return false
	}
	return channelID == m.ChannelID
}
