package main

import (
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// GuildCreate handles the discord guild create event fired when a new guild is made available to the bot
func GuildCreate(s *discordgo.Session, g *discordgo.GuildCreate) {
	_, err := getCurrentChannel(g.ID)
	if err != nil {
		logrus.Infof("Entering room for guild from GUILDCREATE: %s", g.ID)
		enterRandomChannel(s, g.ID)
	}
}
