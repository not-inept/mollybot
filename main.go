package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
	"gitlab.com/not-inept/mollybot/util"
)

func main() {
	logrus.Info("Starting up MollyBot.")
	config := util.LoadConfig("mollybot.json")

	discord, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		logrus.Fatal("Error creating Discord session,", err)
		return
	}
	discord.AddHandler(Ready)
	discord.AddHandler(MessageCreate)
	discord.AddHandler(GuildCreate)
	err = discord.Open()
	if err != nil {
		logrus.Fatal("Error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	logrus.Infof("MollyBot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

}
