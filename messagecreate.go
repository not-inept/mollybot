package main

import (
	"fmt"
	"math/rand"
	"strings"

	"github.com/Necroforger/gomoji"
	"github.com/bwmarrin/discordgo"
)

func myMention(s *discordgo.Session) string {
	// Mentions show up as <@id> in messages where they aren't parsed out
	return fmt.Sprintf("<@%s>", s.State.User.ID)
}

func isPet(s *discordgo.Session, m *discordgo.MessageCreate) bool {
	m.Content = strings.ToLower(m.Content)
	return strings.Contains(m.Content, fmt.Sprintf("pets %s", myMention(s))) ||
		strings.Contains(m.Content, fmt.Sprintf("pet %s", myMention(s)))
}

func openDoor(s *discordgo.Session, m *discordgo.MessageCreate) {
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":door:"))
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":unlock:"))
	unlockDoor(m.ChannelID)
}

func closeDoor(s *discordgo.Session, m *discordgo.MessageCreate) {
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":door:"))
	s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":lock:"))
	lockDoor(m.ChannelID)
}

// MessageCreate will be called (due to AddHandler) every time a new
// message is created on any channel that the authenticated bot has access to.
func MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// logrus.Infof("Content: %s\nMy mention: %s", m.Content, myMention(s))
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.Contains(m.Content, "door") {
		if strings.Contains(m.Content, "open") {
			openDoor(s, m)
		}
		if strings.Contains(m.Content, "close") || strings.Contains(m.Content, "shut") {
			closeDoor(s, m)
		}
	}

	if rand.Float32()*100 < 6.6 {
		enterRandomChannel(s, m.GuildID)
	} else if inCurrentChannel(m) {
		if isPet(s, m) {
			if rand.Float32()*100 > 75 {
				purr(s, m)
			} else if rand.Float32()*100 < 50 {
				meow(s, m)
			} else if rand.Float32()*100 < 5 {
				hide(s, m)
			} else {
				s.MessageReactionAdd(m.ChannelID, m.ID, gomoji.Format(":cat:"))
			}
			return
		} else if rand.Float32()*100 < 7.5 {
			meow(s, m)
		}
	}

	// if m.Content "open the door") {

	// }
}
