package main

import (
	"github.com/bwmarrin/discordgo"
	"github.com/sirupsen/logrus"
)

// Ready handles the discord Ready event fired when the bot connects to the discord API
func Ready(s *discordgo.Session, r *discordgo.Ready) {
	for _, guild := range r.Guilds {
		_, err := getCurrentChannel(guild.ID)
		if err != nil && !guild.Unavailable {
			logrus.Infof("Entering room for guild from READY: %s", guild.ID)
			enterRandomChannel(s, guild.ID)
		}
	}
}
