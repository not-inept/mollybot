package util

import (
	"encoding/json"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

// Config is MollyBot's basic configuration
type Config struct {
	Token string `json:"token"`
}

// LoadConfig reads the provided configuration file
func LoadConfig(filePath string) Config {
	logrus.Infof("Loading %s configuration.", filePath)
	var config Config

	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		logrus.Fatalf("Error: %v", err)
	}

	err = json.Unmarshal(data, &config)
	if err != nil {
		logrus.Fatalf("Error: %v", err)
	}
	return config
}
